﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShipWeaponData{

    [SerializeField]
    private WeaponData _weapon;

    public WeaponData weapon
    {
        get { return _weapon; }
        set { _weapon = value; }
    }

    [SerializeField]
    private float _multiplier;

    public float multiplier
    {
        get { return _multiplier; }
        set { _multiplier = value; }
    }

    public bool fold { get; set; }
}
