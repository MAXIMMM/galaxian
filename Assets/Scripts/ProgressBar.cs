﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour {

    public Image filled;

    private float _fill;

    public float fill
    {
        get { return _fill; }
        set
        {

            _fill = value;
            filled.fillAmount = _fill;
        }
    }

}
