﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Ship>().health += 30;
        Destroy(this);
	}
}
