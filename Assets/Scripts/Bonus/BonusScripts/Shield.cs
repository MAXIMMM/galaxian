﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour {

    private static float time = 4f;
    private float t;

	// Use this for initialization
	void Start () {
        GetComponent<Ship>().shield = true;
        t = time;
	}

    private void Update()
    {
        t -= Time.deltaTime;
        if (t <= 0) Stop();
    }

    void Stop()
    {
        GetComponent<Ship>().shield = false;
        Destroy(this);
    }

    private void OnDisable()
    {
        GetComponent<Ship>().shield = false;
        Destroy(this);
    }
}
