﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedBonus : MonoBehaviour {
    private static float time = 4f;
    private static float deltaSpeed=2f;
    private float t;

    // Use this for initialization
    void Start()
    {
        GetComponent<Ship>().deltaShootSpeed = deltaSpeed;
        t = time;
    }

    private void Update()
    {
        t -= Time.deltaTime;
        if (t <= 0) Stop();
    }

    void Stop()
    {
        GetComponent<Ship>().deltaShootSpeed = 1f;
        Destroy(this);
    }

    private void OnDisable()
    {
        GetComponent<Ship>().deltaShootSpeed = 1f;
        Destroy(this);
    }
}
