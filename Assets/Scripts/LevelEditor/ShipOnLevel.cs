﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipOnLevel : ScriptableObject {

    [SerializeField]
    private List<ShipForSave> _ships;

    public List<ShipForSave> ships
    {
        get
        {
            if (_ships == null) _ships = new List<ShipForSave>();
            return _ships;
        }
        set { _ships = value; }
    }

}

[System.Serializable]
public class ShipForSave
{
    [SerializeField]
    private GameObject _prefab;
    public GameObject prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }

    [SerializeField]
    private Vector3 _position;
    public Vector3 position
    {
        get { return _position; }
        set { _position = value; }
    }

}
