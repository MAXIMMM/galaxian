﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShipType
{
    Player,
    Enemy
}

[System.Serializable]
public class ShipStat {
    
    [SerializeField]
    private string _name = "Ship name";
    public string name
    {
        get { return _name; }
        set { _name = value; }
    }
    
    [SerializeField]
    private ShipType _type = ShipType.Player;
    public ShipType type
    {
        get { return _type; }
        set { _type = value; }
    }
    
    [SerializeField]
    private GameObject _shipPrefab;
    public GameObject shipPrefab
    {
        get { return _shipPrefab; }
        set { _shipPrefab = value; }
    }

    [SerializeField]
    private ShipBaseStat _baseStat;
    public ShipBaseStat baseStat
    {
        get { return _baseStat; }
        set { _baseStat = value; }
    }

    [SerializeField]
    private float _health;
    public float health
    {
        get { return _health; }
        set { _health = value; }
    }

    [SerializeField]
    private float _shootSpeed;
    public float shootSpeed
    {
        get { return _shootSpeed; }
        set { _shootSpeed = value; }
    }

    [SerializeField]
    private float _moveSpeed;
    public float moveSpeed
    {
        get { return _moveSpeed; }
        set { _moveSpeed = value; }
    }

    [SerializeField]
    private float _mobility;
    public float mobility
    {
        get { return _mobility; }
        set { _mobility = value; }
    }

    [SerializeField]
    private List<ShipWeaponData> _weapons;
    public List<ShipWeaponData> weapons
    {
        get
        {
            if (_weapons == null) _weapons = new List<ShipWeaponData>();
            return _weapons;
        }
        set { _weapons = value; }
    }


    public bool fold { get; set; }

}
