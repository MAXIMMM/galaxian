﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;


[CanEditMultipleObjects]
[CustomEditor(typeof(ShipOnLevel))]
public class ShipOnLevelEditor1 : Editor {

    private ShipOnLevel ships;

    private void OnEnable()

    {
        ships = target as ShipOnLevel;
    }


    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Load to Scene"))
        {
            Debug.Log("Load to Scene");

            for (int i = 0; i < ships.ships.Count; i++)
            {

                Quaternion rotation = Quaternion.AngleAxis(180, Vector3.forward);
                Ship ship = Instantiate(ships.ships[i].prefab, ships.ships[i].position, rotation).GetComponent<Ship>();
                ship.shipData.shipPrefab = ships.ships[i].prefab;
            }
        }
    }
}
