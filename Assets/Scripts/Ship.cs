﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : PoolingObject
{

    private float maxHealth;
    private float _health;

    public float deltaShootSpeed { get; set; }


    private GameObject shieldObject;
    private bool _shield = false;
    public bool shield
    {
        get { return _shield; }
        set
        {
            if (shieldObject == null) shieldObject = GameObject.FindGameObjectWithTag("Shield");
            shieldObject.SetActive(value);
            _shield = value;
        }
    }
    

    public float health
    {
        get
        {
            return _health;
        }
        set
        {

            if (shield)
            {
                if (value < health) return;
            }

            _health = value;
            if (progressBar != null)
            {
                if (_health >= maxHealth*0.98f)
                {
                    progressBar.gameObject.SetActive(false);
                }
                else
                {
                    progressBar.gameObject.SetActive(true);
                }
                progressBar.fill = _health / maxHealth;
            }

            if(shipData.type == ShipType.Player)
            {
                GameManager.instance.progressBar.fill = _health / maxHealth;
            }
            if (_health <= 0) Crash();
            if (_health > maxHealth) _health = maxHealth;
        }
    }

    private ProgressBar progressBar;

    private Vector3 delta;

    [SerializeField]
    private ShipStat _shipData;
    public ShipStat shipData
    {
        get { return _shipData; }
        set { _shipData = value; }
    }

    private AudioSource audioSource;

    private void Start()
    {
        progressBar = GetComponentInChildren<ProgressBar>();
        progressBar.gameObject.SetActive(false);
        audioSource = GetComponent<AudioSource>();
    }

    private float[] shootTime;

    public override void OnPop()
    {
        deltaShootSpeed = 1f;

        if (shipData.type == ShipType.Player) shield = false;

        shootTime = new float[shipData.weapons.Count];
        for (int i = 0; i < shootTime.Length; i++)
        {
            shootTime[i] = 1f/(shipData.weapons[i].weapon.shootSpeed * shipData.shootSpeed);
        }

        maxHealth =  health = shipData.health * shipData.baseStat.Health;

        gameObject.SetActive(true);
    }

    public override void OnPush()
    {
        if (GameManager.instance.playerShip == gameObject)
        {
            GameManager.instance.playerShip = null;
        }
        else
        {
            GameManager.instance.enemyShips.Remove(gameObject);
        }

        transform.parent = null;
        gameObject.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
		if(shipData.type== ShipType.Player)
        {
            float speed = shipData.baseStat.moveSpeed * shipData.moveSpeed;

            if (Input.GetAxis(axisName: "Horizontal") > 0.1f)
            {
                delta = transform.right * Time.deltaTime * speed;
            }

            if (Input.GetAxis(axisName: "Horizontal") < -0.1f)
            {
                delta = - transform.right * Time.deltaTime * speed;
            }
            

            if (Input.GetAxis(axisName: "Vertical") > 0.1f)
            {
                delta = transform.up * Time.deltaTime * speed;
            }
            

            if (Input.GetAxis(axisName: "Vertical") < -0.1f)
            {
                delta = - transform.up * Time.deltaTime * speed;
            }

            transform.position += delta;

            if (transform.position.y < GameManager.instance.forPlayer.minScreenY) transform.position -= delta;
            if (transform.position.x < GameManager.instance.forPlayer.minScreenX) transform.position -= delta;
            if (transform.position.y > GameManager.instance.forPlayer.maxScreenY) transform.position -= delta;
            if (transform.position.x > GameManager.instance.forPlayer.maxScreenX) transform.position -= delta;
            

            if (delta.magnitude > 0) delta -= delta.normalized * delta.magnitude * 0.2f;
            else delta = Vector3.zero;


            for (int i = 0; i < shootTime.Length; i++)
            {
                //Debug.Log(shipData.weapons[i].weapon.button);
                shootTime[i] -= Time.deltaTime;
                if (shootTime[i] <= 0)
                {
                    if (Input.GetAxis(axisName: shipData.weapons[i].weapon.button) != 0)
                    {
                        //Debug.Log(shipData.weapons[i].weapon.button);
                        shootTime[i] = (1f / (shipData.weapons[i].weapon.shootSpeed * shipData.shootSpeed)) / deltaShootSpeed;
                        Shot(shipData.weapons[i].weapon.prefab.GetComponent<Weapon>(), shipData.weapons[i].multiplier);
                    }
                }

            }

        }
        else
        {
            for(int i=0; i<shootTime.Length; i++)
            {
                shootTime[i] -= Time.deltaTime;
                if (shootTime[i] <= 0&&Random.value<0.5f)
                {
                    shootTime[i] = 1f / (shipData.weapons[i].weapon.shootSpeed * shipData.shootSpeed);
                    Shot(shipData.weapons[i].weapon.prefab.GetComponent<Weapon>(), shipData.weapons[i].multiplier);
                }

            }

        }
	}

    private void Shot(Weapon weapon, float multiplier)
    {
        GameObject go =  GameManager.instance.objectsPool.Pop(weapon);
        go.transform.position = transform.position + transform.up;
        go.transform.rotation = transform.rotation;

        Weapon w = go.GetComponent<Weapon>();

        w.masterType = shipData.type;
        w.damage = multiplier;
    }

    public void Crash()
    {
        if (shipData.type == ShipType.Enemy) GameManager.instance.SetupBonus(transform.position);
        SoundManager.instance.PlayDestroy();
        Destroy();
    }

    public void Destroy()
    {
        GameManager.instance.objectsPool.Push(this);
    }
}
