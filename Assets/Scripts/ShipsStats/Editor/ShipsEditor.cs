﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;

[CanEditMultipleObjects]
[CustomEditor(typeof(Ships))]
public class ShipsEditor : Editor {

    private Ships ships;
    private bool isWeaponAdd = false;
    private List<string> AllAxes=new List<string>();

    private void OnEnable()
    {
        ships = target as Ships;

        ReadAxes();
    }

    public void ReadAxes()
    {
        var inputManager = AssetDatabase.LoadAllAssetsAtPath("ProjectSettings/InputManager.asset")[0];

        SerializedObject obj = new SerializedObject(inputManager);

        SerializedProperty axisArray = obj.FindProperty("m_Axes");

        AllAxes.Clear();
        
        for (int i = 0; i < axisArray.arraySize; ++i)
        {
            var axis = axisArray.GetArrayElementAtIndex(i);

            string name = axis.FindPropertyRelative("m_Name").stringValue;

            AllAxes.Add(name);
            //int axisVal = axis.FindPropertyRelative("axis").intValue;
            //UnityEngine.UI.InputField.InputType inputType = (UnityEngine.UI.InputField.InputType)axis.FindPropertyRelative("type").intValue;



            //Debug.Log(name);
            //Debug.Log(axisVal);
            //Debug.Log(inputType);
        }
    }

    public override void OnInspectorGUI()
    {
        #region Base Stats
        GUIStyle uIStyle = new GUIStyle();
        uIStyle.fontStyle = FontStyle.Bold;
        uIStyle.alignment = TextAnchor.MiddleCenter;
        EditorGUILayout.BeginVertical((GUIStyle)"ProgressBarBack");

        GUILayout.Label("Base Ships Stats", uIStyle);

        if (GUILayout.Button("Add"))
        {
            Debug.Log("Add");
                       
            ships.baseStats.Add(new ShipBaseStat());
        }

        if (ships.baseStats != null)
        {
            EditorGUI.indentLevel += 1;
            for (int i = 0; i < ships.baseStats.Count; i++)
            {
                EditorGUILayout.BeginHorizontal((GUIStyle)"Box");
                ships.baseStats[i].fold = EditorGUILayout.Foldout(ships.baseStats[i].fold, ships.baseStats[i].name);
                if (GUILayout.Button("Delete", GUILayout.Width(50)))
                {
                    Debug.Log("Delete");

                    ships.baseStats.RemoveAt(i);
                }
                else
                {
                    EditorGUILayout.EndHorizontal();

                    if (ships.baseStats[i].fold)
                    {
                        ships.baseStats[i].name = EditorGUILayout.TextField("Name", ships.baseStats[i].name);
                        ships.baseStats[i].Health = EditorGUILayout.FloatField("Health", ships.baseStats[i].Health);
                        //ships.baseStats[i].shootSpeed = EditorGUILayout.FloatField("Shoot Speed", ships.baseStats[i].shootSpeed);
                        ships.baseStats[i].moveSpeed = EditorGUILayout.FloatField("Move Speed", ships.baseStats[i].moveSpeed);
                        ships.baseStats[i].mobility = EditorGUILayout.FloatField("Mobility", ships.baseStats[i].mobility);
                    }
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        EditorGUILayout.EndVertical();

        #endregion

        #region Weapons

        EditorGUILayout.BeginVertical((GUIStyle)"ProgressBarBack");

        GUILayout.Label("Weapons", uIStyle);

        if (GUILayout.Button("Add"))
        {
            Debug.Log("Add");

            ships.weaponsData.Add(new WeaponData());
        }

        if (ships.weaponsData != null)
        {
            EditorGUI.indentLevel += 1;
            for (int i = 0; i < ships.weaponsData.Count; i++)
            {
                EditorGUILayout.BeginHorizontal((GUIStyle)"Box");
                ships.weaponsData[i].fold = EditorGUILayout.Foldout(ships.weaponsData[i].fold, ships.weaponsData[i].name);

                if (GUILayout.Button("Copy", GUILayout.Width(50)))
                {
                    WeaponData weaponData = new WeaponData();
                    weaponData.name = ships.weaponsData[i].name;
                    weaponData.damage = ships.weaponsData[i].damage;
                    weaponData.shootSpeed = ships.weaponsData[i].shootSpeed;
                    weaponData.button = ships.weaponsData[i].button;
                    weaponData.sound = ships.weaponsData[i].sound;

                    ships.weaponsData.Add(weaponData);
                }

                if (GUILayout.Button("Delete", GUILayout.Width(50)))
                {
                    Debug.Log("Delete");

                    ships.weaponsData.RemoveAt(i);
                }
                else
                {
                    EditorGUILayout.EndHorizontal();

                    if (ships.weaponsData[i].fold)
                    {
                        ships.weaponsData[i].name = EditorGUILayout.TextField("Name", ships.weaponsData[i].name);
                        ships.weaponsData[i].prefab = (GameObject)EditorGUILayout.ObjectField("Weapon Prefab", ships.weaponsData[i].prefab, typeof(GameObject), false);
                        ships.weaponsData[i].damage = EditorGUILayout.FloatField("Damage", ships.weaponsData[i].damage);
                        ships.weaponsData[i].shootSpeed = EditorGUILayout.FloatField("Shoot Speed", ships.weaponsData[i].shootSpeed);

                        int cur = 0;

                        if (AllAxes.Contains(ships.weaponsData[i].button)) cur = AllAxes.IndexOf(ships.weaponsData[i].button);

                        cur = EditorGUILayout.Popup("Axis", cur, AllAxes.ToArray());

                        ships.weaponsData[i].button = AllAxes[cur];

                        ships.weaponsData[i].sound = (AudioClip)EditorGUILayout.ObjectField("Sound", ships.weaponsData[i].sound, typeof(AudioClip), false);

                    }
                }

                if (ships.weaponsData[i].prefab != null)
                {
                    if (ships.weaponsData[i].prefab.GetComponent<Rigidbody2D>() == null)
                    {
                        Rigidbody2D rb = ships.weaponsData[i].prefab.AddComponent<Rigidbody2D>();
                        rb.isKinematic = true;
                    }
                        ships.weaponsData[i].prefab.GetComponent<Weapon>().weaponData = ships.weaponsData[i];
                    
                    if(ships.weaponsData[i].prefab.GetComponent<AudioSource>() == null)
                    {
                        AudioSource audioSource = ships.weaponsData[i].prefab.AddComponent<AudioSource>();
                        audioSource.playOnAwake = true;
                        audioSource.clip = ships.weaponsData[i].sound;
                    }
                    else
                    {
                        ships.weaponsData[i].prefab.GetComponent<AudioSource>().clip = ships.weaponsData[i].sound;
                    }
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        EditorGUILayout.EndVertical();

        #endregion

        #region Ships

        EditorGUILayout.BeginVertical((GUIStyle)"ProgressBarBack");

        GUILayout.Label("Ships", uIStyle);

        if (GUILayout.Button("Add"))
        {
            Debug.Log("Add");

            ships.shipStats.Add(new ShipStat());
        }

        if (ships.shipStats != null)
        {
            string[] baseStatsString = new string[ships.baseStats.Count];

            for (int i = 0; i < baseStatsString.Length; i++)
            {
                baseStatsString[i] = ships.baseStats[i].name;
            }

            string[] baseWeaponString = new string[ships.weaponsData.Count];

            for (int i = 0; i < baseWeaponString.Length; i++)
            {
                baseWeaponString[i] = ships.weaponsData[i].name;
            }

            EditorGUI.indentLevel += 1;
            for (int i = 0; i < ships.shipStats.Count; i++)
            {
                EditorGUILayout.BeginHorizontal((GUIStyle)"Box");
                ships.shipStats[i].fold = EditorGUILayout.Foldout(ships.shipStats[i].fold, ships.shipStats[i].name);
                if (ships.shipStats[i].shipPrefab != null&& ships.shipStats[i].type == ShipType.Enemy)
                {
                    if (GUILayout.Button("To Scene", GUILayout.Width(70)))
                    {
                        Quaternion rotation = Quaternion.AngleAxis(180, Vector3.forward);
                        Ship ship = Instantiate(ships.shipStats[i].shipPrefab, Vector3.zero, rotation).GetComponent<Ship>();
                        ship.shipData.shipPrefab = ships.shipStats[i].shipPrefab;
                    }
                    EditorGUILayout.LabelField("", GUILayout.Width(30));
                }
                if (GUILayout.Button("Copy", GUILayout.Width(50)))
                {
                    Debug.Log("Copy");

                    ShipStat shipStat = new ShipStat();
                    shipStat.name = ships.shipStats[i].name;
                    shipStat.type = ships.shipStats[i].type;
                    shipStat.baseStat = ships.shipStats[i].baseStat;
                    shipStat.health = ships.shipStats[i].health;
                    shipStat.shootSpeed = ships.shipStats[i].shootSpeed;
                    shipStat.moveSpeed = ships.shipStats[i].moveSpeed;
                    shipStat.mobility = ships.shipStats[i].mobility;

                    ships.shipStats.Add(shipStat);
                }
                if (GUILayout.Button("Delete", GUILayout.Width(50)))
                {
                    Debug.Log("Delete");

                    ships.shipStats.RemoveAt(i);
                }
                else {
                    EditorGUILayout.EndHorizontal();

                    if (ships.shipStats[i].fold)
                    {
                        EditorGUI.indentLevel += 1;

                        ships.shipStats[i].name = EditorGUILayout.TextField("Name", ships.shipStats[i].name);
                        ships.shipStats[i].type = (ShipType)EditorGUILayout.EnumPopup("Type", ships.shipStats[i].type);
                        ships.shipStats[i].shipPrefab = (GameObject)EditorGUILayout.ObjectField("Ship Prefab", ships.shipStats[i].shipPrefab, typeof(GameObject), false);

                        GUILayout.Label("Ship Stats", uIStyle);

                        if (ships.baseStats.Count > 0)
                        {
                            int cur = 0;

                            if (ships.baseStats.Contains(ships.shipStats[i].baseStat)) cur = ships.baseStats.IndexOf(ships.shipStats[i].baseStat);

                            cur = EditorGUILayout.Popup("Base Stat", cur, baseStatsString);

                            ships.shipStats[i].baseStat = ships.baseStats[cur];

                        }
                        else
                        {
                            EditorGUILayout.HelpBox("Requires at least one Base Stat", MessageType.Error, true);
                        }

                        EditorGUILayout.BeginHorizontal();
                        ships.shipStats[i].health = EditorGUILayout.FloatField("Health", ships.shipStats[i].health);
                        EditorGUILayout.LabelField("x " + ships.shipStats[i].baseStat.Health.ToString() + "=" + (ships.shipStats[i].health * ships.shipStats[i].baseStat.Health).ToString());
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        ships.shipStats[i].moveSpeed = EditorGUILayout.FloatField("Move Speed", ships.shipStats[i].moveSpeed);
                        EditorGUILayout.LabelField("x " + ships.shipStats[i].baseStat.moveSpeed.ToString() + "=" + (ships.shipStats[i].moveSpeed * ships.shipStats[i].baseStat.moveSpeed).ToString());
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();
                        ships.shipStats[i].mobility = EditorGUILayout.FloatField("Mobility", ships.shipStats[i].mobility);
                        EditorGUILayout.LabelField("x " + ships.shipStats[i].baseStat.mobility.ToString() + "=" + (ships.shipStats[i].mobility * ships.shipStats[i].baseStat.mobility).ToString());
                        EditorGUILayout.EndHorizontal();

                        GUILayout.Label("Weapons", uIStyle);

                        EditorGUILayout.BeginHorizontal();
                        ships.shipStats[i].shootSpeed = EditorGUILayout.FloatField("Shoot Speed multiplier", ships.shipStats[i].shootSpeed);
                        //EditorGUILayout.LabelField("x " + ships.shipStats[i].baseStat.shootSpeed.ToString() + "=" + (ships.shipStats[i].shootSpeed * ships.shipStats[i].baseStat.shootSpeed).ToString());
                        EditorGUILayout.EndHorizontal();

                        EditorGUILayout.BeginHorizontal();

                        EditorGUILayout.LabelField("", GUILayout.Width(50));

                        if (GUILayout.Button("Add"))
                        {
                            Debug.Log("Add");
                            if(ships.weaponsData.Count > 0)
                            {
                                ShipWeaponData shipWeaponData = new ShipWeaponData();

                                shipWeaponData.weapon = ships.weaponsData[0];

                                ships.shipStats[i].weapons.Add(shipWeaponData);
                            }
                        }

                        EditorGUILayout.LabelField("", GUILayout.Width(50));

                        EditorGUILayout.EndHorizontal();

                        if (ships.weaponsData.Count == 0)
                        {
                            EditorGUILayout.HelpBox("Requires at least one Weapon", MessageType.Error, true);
                        }

                        for (int j = 0; j < ships.shipStats[i].weapons.Count; j++)
                        {
                            EditorGUI.indentLevel += 1;
                            EditorGUILayout.BeginHorizontal();
                            ships.shipStats[i].weapons[j].fold = EditorGUILayout.Foldout(ships.shipStats[i].weapons[j].fold, ships.shipStats[i].weapons[j].weapon.name);
                            EditorGUILayout.LabelField("", GUILayout.Width(5));
                            ships.shipStats[i].weapons[j].multiplier = EditorGUILayout.FloatField(ships.shipStats[i].weapons[j].multiplier);
                            EditorGUILayout.LabelField("x " + ships.shipStats[i].weapons[j].weapon.damage.ToString() + " = " + (ships.shipStats[i].weapons[j].weapon.damage * ships.shipStats[i].weapons[j].multiplier).ToString());
                            //EditorGUILayout.LabelField("sh/s= " + (ships.shipStats[i].weapons[j].weapon.shootSpeed * ships.shipStats[i].shootSpeed).ToString(), GUILayout.Width(60));
                            if (GUILayout.Button("-", GUILayout.Width(20)))
                            {
                                Debug.Log("Delete");

                                ships.shipStats[i].weapons.RemoveAt(j);
                            }
                            else
                            {
                                EditorGUILayout.EndHorizontal();

                                if (ships.shipStats[i].weapons[j].fold)
                                {
                                    int cur = 0;

                                    if (ships.weaponsData.Contains(ships.shipStats[i].weapons[j].weapon)) cur = ships.weaponsData.IndexOf(ships.shipStats[i].weapons[j].weapon);

                                    cur = EditorGUILayout.Popup("Base Weapon", cur, baseWeaponString);

                                    ships.shipStats[i].weapons[j].weapon = ships.weaponsData[cur];

                                }
                            }
                            EditorGUI.indentLevel -= 1;
                        }

                        EditorGUI.indentLevel -= 1;
                    }
                    
                }

                if (ships.shipStats[i].shipPrefab != null)
                {
                    if (ships.shipStats[i].shipPrefab.GetComponent<Rigidbody2D>() == null)
                    {
                        Rigidbody2D rb = ships.shipStats[i].shipPrefab.AddComponent<Rigidbody2D>();
                        rb.isKinematic = true;
                    }

                    if (ships.shipStats[i].shipPrefab.GetComponent<Ship>() == null)
                    {
                        Ship wp = ships.shipStats[i].shipPrefab.AddComponent<Ship>();
                        wp.shipData = ships.shipStats[i];
                    }
                    else
                    {
                        Ship wp = ships.shipStats[i].shipPrefab.GetComponent<Ship>();
                        wp.shipData = ships.shipStats[i];
                    }

                    if (ships.shipStats[i].shipPrefab.GetComponent<AudioSource>() == null)
                    {
                        AudioSource audioSource = ships.shipStats[i].shipPrefab.AddComponent<AudioSource>();
                        audioSource.playOnAwake = false;
                    }
                }
            }

            EditorGUI.indentLevel -= 1;
        }

        EditorGUILayout.EndVertical();

        #endregion

        EditorUtility.SetDirty(target);

    }

    
}
