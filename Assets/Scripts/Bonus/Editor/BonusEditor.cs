﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(BonusSystem))]
public class BonusEditor : Editor {

    private BonusSystem bonusSystem;

    private void OnEnable()

    {
        bonusSystem = target as BonusSystem;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Add"))
        {
            bonusSystem.bonuses.Add(new BonusData());
        }

        EditorGUI.indentLevel += 1;
        for (int i = 0; i < bonusSystem.bonuses.Count; i++)
        {
            EditorGUILayout.BeginHorizontal("Box");

            bonusSystem.bonuses[i].fold = EditorGUILayout.Foldout(bonusSystem.bonuses[i].fold, bonusSystem.bonuses[i].name);

            if (GUILayout.Button("Delete", GUILayout.Width(50)))
            {
                Debug.Log("Delete");

                bonusSystem.bonuses.RemoveAt(i);
            }
            else
            {
                EditorGUILayout.EndHorizontal();

                if (bonusSystem.bonuses[i].fold)
                {
                    EditorGUI.indentLevel += 1;

                    bonusSystem.bonuses[i].name = EditorGUILayout.TextField("Name", bonusSystem.bonuses[i].name);
                    bonusSystem.bonuses[i].script = (Object)EditorGUILayout.ObjectField("Script", bonusSystem.bonuses[i].script, typeof(Object), false);
                    bonusSystem.bonuses[i].prefab = (GameObject)EditorGUILayout.ObjectField("Prefab", bonusSystem.bonuses[i].prefab, typeof(GameObject), false);

                    EditorGUI.indentLevel -= 1;
                }

            }
        }

        EditorGUI.indentLevel -= 1;

        EditorUtility.SetDirty(target);
    }
}
