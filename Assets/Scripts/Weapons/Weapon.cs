﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : PoolingObject
{

    private ShipType _masterType;

    public ShipType masterType
    {
        get { return _masterType; }
        set { _masterType = value; }
    }

    private float _damage;

    public float damage
    {
        get { return _damage; }
        set
        {
            _damage = value;
            _damage *= weaponData.damage;
        }
    }



    protected float lifeTime;

    [SerializeField]
    private WeaponData _weaponData;
    public WeaponData weaponData
    {
        get { return _weaponData; }
        set { _weaponData = value; }
    }

    public override void OnPop()
    {
        
    }

    public override void OnPush()
    {
        
    }

    public void Destroy()
    {
        GameManager.instance.objectsPool.Push(this);
    }

    protected virtual void OnMissed()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Ship ship = collision.gameObject.GetComponent<Ship>();

        if (ship != null)
        {
            if (masterType != ship.shipData.type)
            {
                //Debug.Log("Destroy");

                float percent = 100 - ship.shipData.baseStat.mobility * ship.shipData.mobility;
                float chance = Random.Range(0, 100);

                if (chance <= percent)
                { 
                    ship.health -= damage;
                    Destroy();
                }
                else
                {
                    OnMissed();
                }
            }
        }
        else
        {
            if (masterType == ShipType.Player)
            {
                Bonus bonus = collision.gameObject.GetComponent<Bonus>();

                if (bonus != null)
                {
                    bonus.GetBonus();
                }
            }
        }
    }
}
