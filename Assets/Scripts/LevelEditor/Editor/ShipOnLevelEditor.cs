﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ShipOnLevelEditor {
    [MenuItem("Galaxian/Create New Level")]
    public static void CreateMyAsset()
    {
        ShipOnLevel asset = ScriptableObject.CreateInstance<ShipOnLevel>();

        asset.ships = new List<ShipForSave>();

        Ship[] ships = GameObject.FindObjectsOfType<Ship>();

        for(int i=0; i<ships.Length; i++)
        {
            if (ships[i].shipData.type == ShipType.Enemy)
            {
                ShipForSave shipForSave = new ShipForSave();

                shipForSave.prefab = ships[i].shipData.shipPrefab;
                shipForSave.position = ships[i].transform.position;

                asset.ships.Add(shipForSave);
            }
        }

        AssetDatabase.CreateAsset(asset, "Assets/Levels/NewLevel.asset");
        AssetDatabase.SaveAssets();

        EditorUtility.FocusProjectWindow();

        Selection.activeObject = asset;
    }
}
