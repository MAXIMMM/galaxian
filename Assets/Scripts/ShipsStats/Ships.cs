﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ShipsData", menuName = "Galaxy/New Ships Config")]
public class Ships : ScriptableObject {
    [SerializeField]
    private List<ShipBaseStat> _baseStats;
    public List<ShipBaseStat> baseStats
    {
        get
        {
            if (_baseStats == null) _baseStats = new List<ShipBaseStat>();
            return _baseStats;
        }
        set { _baseStats = value; }
    }

    [SerializeField]
    private List<ShipStat> _shipStats;
    public List<ShipStat> shipStats
    {
        get
        {
            if (_shipStats == null) _shipStats = new List<ShipStat>();
            return _shipStats;
        }
        set { _shipStats = value; }
    }

    [SerializeField]
    private List<WeaponData> _weaponsData;
    public List<WeaponData> weaponsData
    {
        get
        {
            if (_weaponsData == null) _weaponsData = new List<WeaponData>();
            return _weaponsData;
        }
        set { _weaponsData = value; }
    }


}
