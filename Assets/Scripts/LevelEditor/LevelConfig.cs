﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LevelConfig {

    [SerializeField]
    private string _name= "Level";
    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private ScriptableObject _nexus;
    public ScriptableObject nexus
    {
        get { return _nexus; }
        set { _nexus = value; }
    }

    [SerializeField]
    private Sprite _background;
    public Sprite background
    {
        get { return _background; }
        set { _background = value; }
    }

    [SerializeField]
    private AudioClip _music;
    public AudioClip music
    {
        get { return _music; }
        set { _music = value; }
    }

    [SerializeField]
    private float _separationTime;

    public float separationTime
    {
        get { return _separationTime; }
        set { _separationTime = value; }
    }


    public bool fold { get; set; }
}
