﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool {

    private Dictionary<GameObject, List<GameObject>> pool;
    
    public ObjectsPool()
    {
        pool = new Dictionary<GameObject, List<GameObject>>();
    }

    public void Push(PoolingObject obj)
    {
        obj.OnPush();

        if (pool.ContainsKey(obj.Key))
        {
            pool[obj.Key].Add(obj.GetGameObject());
        }
        else
        {
            List<GameObject> list = new List<GameObject>();
            list.Add(obj.GetGameObject());
            pool.Add(obj.Key, list);
        }
    }

    public GameObject Pop(PoolingObject obj)
    {
        GameObject Key = obj.Key;
        if (Key == null) Key = obj.gameObject;

        if (pool.ContainsKey(Key))
        {
            if (pool[Key].Count > 0)
            {
                //Debug.Log("Get");

                List<GameObject> list = pool[Key];

                GameObject go = list[0];
                list.RemoveAt(0);
                PoolingObject p = go.GetComponent<PoolingObject>();
                p.OnPop();

                return go;
            }
            else
            {
                return Create(obj);
            }
        }
        else
        {
            return Create(obj);
        }
    }

    public GameObject Create(PoolingObject obj)
    {
        //Debug.Log("Instantiate");
        GameObject go = GameObject.Instantiate(obj.gameObject);
        PoolingObject p = go.GetComponent<PoolingObject>();
        p.Key = obj.gameObject;
        p.OnPop();
        return go;
    }
}


