﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager instance;

    [Header("Audio Saurces")]

    public AudioSource Music;
    public AudioSource Sound;

    [Header("GUI Sounds")]
    public AudioClip defaultMusic;
    public AudioClip click;

    [Header("Game Sound")]
    public AudioClip destroySound;
    public AudioClip bonusSound;
    public AudioClip getBonusSound;

    private void Awake()
    {
        instance = this;
    }


    public void SetMusic(AudioClip audioClip = null)
    {
        if (((audioClip != null) && (Music.clip != audioClip)) || ((audioClip==null)&&(Music.clip!=defaultMusic)))
        {
            if (Music.isPlaying) Music.Stop();
            if (audioClip == null)
            {
                Music.clip = defaultMusic;
            }
            else
            {
                Music.clip = audioClip;
            }

            Music.Play();
        }
    }

    public void PlaySound(AudioClip audioClip)
    {
        if (Sound.isPlaying) Sound.Stop();
        Sound.clip = audioClip;
        Sound.Play();
    }

    public void Click()
    {
        PlaySound(click);
    }

    public void PlayDestroy()
    {
        PlaySound(destroySound);
    }

    public void PlayBonus()
    {
        PlaySound(bonusSound);
    }

    public void PlayGetBonus()
    {
        PlaySound(getBonusSound);
    }

	// Use this for initialization
	void Start () {
        SetMusic();
	}
}
