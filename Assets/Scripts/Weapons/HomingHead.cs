﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingHead : MonoBehaviour {

    private Missile master;


	// Use this for initialization
	void Start () {
        master = GetComponentInParent<Missile>();
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Ship ship = collision.gameObject.GetComponent<Ship>();

        if (ship != null)
        {
            if (master.masterType != ship.shipData.type)
            {
                //Debug.Log("enter");
                if (master.target == null) master.target = ship.transform;
            }
        }
    }
}
