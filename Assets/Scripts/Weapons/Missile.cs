﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Weapon {

    private Transform _target;

    public Transform target
    {
        get { return _target; }
        set { _target = value; }
    }


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (target != null)
        {
            transform.Rotate(transform.forward, Vector2.SignedAngle(transform.up, target.transform.position - transform.position)*2*Time.deltaTime);
        }


        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0)
        {
            Destroy();
        }
        transform.position += 3f * transform.up * Time.deltaTime;
    }

    protected override void OnMissed()
    {
        target = null;
    }

    public override void OnPop()
    {
        gameObject.SetActive(true);
        lifeTime = 5f;
    }

    public override void OnPush()
    {
        //Debug.Log("Push");
        target = null;
        //Debug.Log("Push");
        gameObject.SetActive(false);
    }
}
