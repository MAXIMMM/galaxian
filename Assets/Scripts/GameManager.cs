﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    #region Object Pool

    private ObjectsPool _objectsPool;
    public ObjectsPool objectsPool
    {
        get
        {
            if (_objectsPool == null) _objectsPool = new ObjectsPool();
            return _objectsPool;
        }
    }


    #endregion

    #region State Machine

    public enum GameState
    {
        Loading,
        Menu,
        Game,
        End
    }

    private enum GameResult
    {
        Win,
        Lose,
        Restart
    }

    private GameResult gameResult;

    public GameState currentState
    {
        get
        {
            return stateMachine.CurrentState;
        }
        set
        {
            if (stateMachine == null)
                InitStateMachine();

            stateMachine.CurrentState = value;
        }
    }

    private StateMachine<GameState> stateMachine;

    private void InitStateMachine()
    {
        stateMachine = new StateMachine<GameState>();

        stateMachine.AddState(GameState.Loading, OnLoadingStart, OnLoadingUpdate, OnLoadingEnd);
        stateMachine.AddState(GameState.Menu, OnMenuStart, OnMenuUpdate, OnMenuEnd);
        stateMachine.AddState(GameState.Game, OnGameStart, OnGameUpdate, OnGameEnd);
        stateMachine.AddState(GameState.End, OnEndStart, OnEndUpdate, OnEndEnd);
    }

    #endregion

    [Header("Game Settings")]
    public ScriptableObject shipsSettings;
    public ScriptableObject levelsSettings;
    public ScriptableObject bonusSettings;

    private List<GameObject> playerShips;

    private List<GameObject> _enemyShips;
    public List<GameObject> enemyShips
    {
        get
        {
            if (_enemyShips == null) _enemyShips = new List<GameObject>();
            return _enemyShips;
        }
        set
        {
            _enemyShips = value;
        }
    }

    private List<GameObject> _bonuses;
    public List<GameObject> bonuses
    {
        get
        {
            if (_bonuses == null) _bonuses = new List<GameObject>();
            return _bonuses;
        }
        set
        {
            _bonuses = value;
        }
    }

    private Vector2 containerPosition;

    public int currentShip
    {
        get
        {
            return PlayerPrefs.GetInt("CurrentShip", 0);
        }
        set
        {
            int s = value;

            DrawMenuShips(s);

            PlayerPrefs.SetInt("CurrentShip", s);
        }
    }

    public int currentLevel
    {
        set
        {
            LevelsData levelsData = (LevelsData)levelsSettings;
            int count = levelsData.levels.Count;

            int l = value;

            if (l >= count) l = 0;

            PlayerPrefs.SetInt("CurrentLevel", l);
        }

        get
        {
            return PlayerPrefs.GetInt("CurrentLevel", 0);
        }
    }

    
    [Header("Menu")]
    public Transform menuSpawnPoint;
    private List<GameObject> menuShips;

    [Header("Game")]
    
    public Transform playerSpawnPoint;
    public ProgressBar progressBar;
    public Image background;

    public GameObject playerShip { get; set; }

    [Range(0, 1)]
    public float bonusChance = 0.5f;

    private GameObject _shipsContainer;
    public GameObject shipsContainer
    {
        get { return _shipsContainer; }
        set { _shipsContainer = value; }
    }


    private GameObject _attackContainer;

    public GameObject attackContainer
    {
        get
        {
            if (_attackContainer == null)
            {
                _attackContainer = new GameObject("container");
                ShipContainer sc = _attackContainer.AddComponent<ShipContainer>();
                sc.player = playerShip.transform;
            }
            return _attackContainer;
        }
        set { _attackContainer = value; }
    }


    private float sepTime, sTime;

    [System.Serializable]
    public class ScreenRect
    {
        public float minScreenX;
        public float maxScreenX;
        public float minScreenY;
        public float maxScreenY;
    }

    public ScreenRect forEnemy;
    
    public ScreenRect forPlayer;

    private float minX, maxX;
    private float minY, maxY;


    [Header("UI")]
    public GameObject LoadingScreen;
    public GameObject MenuScreen;
    public GameObject GameScreen;
    public GameObject PauseScreen;
    public GameObject WinScreen;
    public GameObject LoseScreen;

    private void Awake()
    {
        instance = this;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(new Vector3(forEnemy.minScreenX + (forEnemy.maxScreenX - forEnemy.minScreenX)/2f, forEnemy.minScreenY + (forEnemy.maxScreenY - forEnemy.minScreenY) / 2f, 0f), 
                            new Vector3(forEnemy.maxScreenX - forEnemy.minScreenX, forEnemy.maxScreenY - forEnemy.minScreenY, 0f));

        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(new Vector3(forPlayer.minScreenX + (forPlayer.maxScreenX - forPlayer.minScreenX) / 2f, forPlayer.minScreenY + (forPlayer.maxScreenY - forPlayer.minScreenY) / 2f, 0f),
                            new Vector3(forPlayer.maxScreenX - forPlayer.minScreenX, forPlayer.maxScreenY - forPlayer.minScreenY, 0f));

        //Gizmos.DrawSphere(shipsContainer.transform.position, 1f);

        //Gizmos.color = Color.red;
        //Gizmos.DrawWireCube(new Vector3(minX + (maxX - minX) / 2f, minY + (maxY - minY) / 2f, 0f), new Vector3(maxX - minX, maxY - minY, 0f));

        //Gizmos.DrawSphere(new Vector3((maxX - minX) / 2f + minX, (maxY - minY) / 2f + minY, 0f), 1f);


    }

    // Use this for initialization
    private void Start()
    {
        currentState = GameState.Loading;
    }

    public void SetupBonus(Vector3 position)
    {
        if (Random.value < bonusChance)
        {
            List<BonusData> bonus = ((BonusSystem)bonusSettings).bonuses;
            int n = Random.Range(0, bonus.Count);

            GameObject go = objectsPool.Pop(bonus[n].prefab.GetComponent<PoolingObject>());
            go.transform.position = position;
            go.GetComponent<Bonus>().data = bonus[n];

            bonuses.Add(go);
        }
    }

    private void GetResult()
    {
        bool enemy = true;
        if (enemyShips.Count == 0||(attackContainer.transform.childCount==0&&shipsContainer.transform.childCount==0)) enemy = false;

        bool player = true;
        if (playerShip == null) player = false;

        if (enemy && player)
        {
            gameResult = GameResult.Restart;
        }
        else if (!enemy && player)
        {
            gameResult = GameResult.Win;
        }
        else if(enemy && !player)
        {
            gameResult = GameResult.Lose;
        }
    }

    private void LoadLevel(int level)
    {
        LevelsData levelsData = (LevelsData)levelsSettings;
        LevelConfig levelConfig = levelsData.levels[level];
        ShipOnLevel shipOnLevel = (ShipOnLevel)levelConfig.nexus;

        background.sprite = levelConfig.background;
        SoundManager.instance.SetMusic(levelConfig.music);

        sepTime = levelsData.levels[level].separationTime;
        sTime = sepTime;

        minX = 0;
        maxX = 0;

        float y0 = shipOnLevel.ships[0].position.y;
        float x0 = shipOnLevel.ships[0].position.x;

        minY = y0;
        maxY = y0;


        shipsContainer = new GameObject("Conteiner");
        shipsContainer.transform.position = new Vector3(0f, forEnemy.minScreenY + (forEnemy.maxScreenY - forEnemy.minScreenY) / 2f, 0f);


        for (int i = 0; i < shipOnLevel.ships.Count; i++)
        {

            Quaternion rotation = Quaternion.AngleAxis(180, Vector3.forward);

            ShipForSave shipForSave = shipOnLevel.ships[i];

            Ship ship = objectsPool.Pop(shipForSave.prefab.GetComponent<PoolingObject>()).GetComponent<Ship>();

            enemyShips.Add(ship.gameObject);
            ship.transform.SetParent(shipsContainer.transform);

            ship.transform.position = shipForSave.position;

            if (shipForSave.position.x < 0)
            {
                if (minX > shipForSave.position.x) minX = shipForSave.position.x;
            }
            else
            {
                if (maxX < shipForSave.position.x) maxX = shipForSave.position.x;
            }

            if (shipForSave.position.y < y0)
            {
                if (minY > shipForSave.position.y) minY = shipForSave.position.y;
            }
            else
            {
                if (maxY < shipForSave.position.y) maxY = shipForSave.position.y;
            }

            ship.transform.rotation = rotation;
        }

        float r = ((maxY - minY) / 2f + minY) - shipsContainer.transform.position.y;

        minX = forEnemy.minScreenX - minX;
        maxX = forEnemy.maxScreenX - maxX;

        float c = (maxY - minY) / 2f;

        minY = forEnemy.minScreenY + c;
        maxY = forEnemy.maxScreenY - c;

        //Debug.Log("R=" + r);

        minY -= r;
        maxY -= r;
    }

    #region Menu

    public void DrawMenuShips(int cur)
    {
        for(int i=0; i<menuShips.Count; i++)
        {
            menuShips[i].SetActive(cur == i);
        }
    }

    public void NextShip()
    {
        currentShip++;

        if (currentShip >= menuShips.Count) currentShip = 0;
    }

    public void PrevShip()
    {
        currentShip--;

        if (currentShip < 0) currentShip = menuShips.Count-1;
    }

    public void Continue()
    {
        currentState = GameState.Game;
    }

    public void NewGame()
    {
        currentLevel = 0;
        currentState = GameState.Game;
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void SetPause(bool isPause)
    {
        if (isPause)
        {
            Time.timeScale = 0f;
            PauseScreen.SetActive(true);
        }
        else
        {
            PauseScreen.SetActive(false);
            Time.timeScale = 1f;
        }
    }

    public void ToMenu()
    {
        currentState = GameState.Menu;
    }

    public void NextLevel()
    {
        currentLevel++;
        currentState = GameState.Game;
    }

    #endregion

    private void LevelReset()
    {
        //Destroy(shipsContainer);
        foreach (GameObject ship in enemyShips.ToArray())
        {
            ship.GetComponent<Ship>().Destroy();
        }

        foreach (GameObject bonus in bonuses.ToArray())
        {
            bonus.GetComponent<Bonus>().Destroy();
        }

        if (playerShip != null) playerShip.GetComponent<Ship>().Destroy();

        Weapon[] weapons = FindObjectsOfType<Weapon>();

        foreach(Weapon weapon in weapons)
        {
            weapon.Destroy();
        }
        //Destroy(attackContainer);
        //Destroy(shipsContainer);
    }

    // Update is called once per frame
    private void Update()
    {
        if(stateMachine!=null) stateMachine.Update();
    }

    #region Loading

    private void OnLoadingStart()
    {
        Time.timeScale = 1f;
        LoadingScreen.SetActive(true);

        menuShips = new List<GameObject>();
        playerShips = new List<GameObject>();
        Ships ships = (Ships)shipsSettings;

        for(int i=0; i<ships.shipStats.Count; i++)
        {
            if(ships.shipStats[i].type== ShipType.Player)
            {
                GameObject s = Instantiate(ships.shipStats[i].shipPrefab, menuSpawnPoint.position, menuSpawnPoint.rotation);
                playerShips.Add(ships.shipStats[i].shipPrefab);


                Destroy(s.GetComponent<Ship>());
                GameObject.FindGameObjectWithTag("Shield").SetActive(false);

                menuShips.Add(s);
            }
        }

        currentShip=currentShip;

        currentState = GameState.Menu;
    }

    private void OnLoadingUpdate()
    {

    }

    private void OnLoadingEnd()
    {
        
    }

    #endregion

    #region Menu

    private void OnMenuStart()
    {
        SoundManager.instance.SetMusic();
        MenuScreen.SetActive(true);
        LoadingScreen.SetActive(false);
        GameScreen.SetActive(false);
        PauseScreen.SetActive(false);
        WinScreen.SetActive(false);
    }

    private void OnMenuUpdate()
    {

    }

    private void OnMenuEnd()
    {
        LoadingScreen.SetActive(true);
        MenuScreen.SetActive(false);
    }

    #endregion

    #region Game

    private void OnGameStart()
    {
        Time.timeScale = 1f;

        LoadLevel(currentLevel);
        ShipContainer sc = shipsContainer.AddComponent<ShipContainer>();
        sc.SetRect(minX, maxX, minY, maxY);

        

        playerShip = objectsPool.Pop(playerShips[currentShip].GetComponent<PoolingObject>());
        playerShip.transform.position = playerSpawnPoint.position;
        playerShip.transform.rotation = playerSpawnPoint.rotation;

        GameScreen.SetActive(true);
        LoadingScreen.SetActive(false);
        MenuScreen.SetActive(false);
        PauseScreen.SetActive(false);
        WinScreen.SetActive(false);
    }

    private void OnGameUpdate()
    {
        sTime -= Time.deltaTime;
        if (attackContainer.transform.childCount == 0)
        {
            if (sTime < 0)
            {
                if (shipsContainer.transform.childCount > 0)
                {
                    sTime = sepTime;

                    GameObject ship = shipsContainer.transform.GetChild(Random.Range(0, shipsContainer.transform.childCount)).gameObject;

                    attackContainer.transform.position = ship.transform.position;

                    attackContainer.GetComponent<ShipContainer>().AddShip(ship);
                }
            }
        }

        //Debug.Log("Count = " + enemyShips.Count);
        if (enemyShips.Count == 0||playerShip==null||(shipsContainer.transform.childCount==0&&attackContainer.transform.childCount==0)) currentState = GameState.End;


    }

    private void OnGameEnd()
    {
        GetResult();
        LevelReset();
    }

    #endregion

    

    #region End

    private void OnEndStart()
    {
        Time.timeScale = 1f;
        GameScreen.SetActive(false);
        MenuScreen.SetActive(false);
        LoadingScreen.SetActive(false);
        PauseScreen.SetActive(false);
        if(gameResult==GameResult.Win) WinScreen.SetActive(true);
        if (gameResult == GameResult.Lose) LoseScreen.SetActive(true);
    }

    private void OnEndUpdate()
    {

    }

    private void OnEndEnd()
    {
        WinScreen.SetActive(false);
        LoseScreen.SetActive(false);
        LoadingScreen.SetActive(true);
    }

    #endregion

}