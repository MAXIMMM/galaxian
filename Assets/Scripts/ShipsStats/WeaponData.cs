﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class WeaponData {

    [SerializeField]
    private string _name = "Weapon";
    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private GameObject _prefab;
    public GameObject prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }

    [SerializeField]
    private float _damage;
    public float damage
    {
        get { return _damage; }
        set { _damage = value; }
    }

    [SerializeField]
    private float _shootSpeed = 1;
    public float shootSpeed
    {
        get { return _shootSpeed; }
        set { _shootSpeed = value; }
    }

    [SerializeField]
    private string _button;
    public string button
    {
        get { return _button; }
        set { _button = value; }
    }

    [SerializeField]
    private AudioClip _sound;
    public AudioClip sound
    {
        get { return _sound; }
        set { _sound = value; }
    }

    public bool fold { get; set; }
}
