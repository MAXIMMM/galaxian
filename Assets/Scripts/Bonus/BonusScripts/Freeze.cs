﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freeze : MonoBehaviour {

    private float time = 4f;


    // Use this for initialization
    void Start()
    {
        GameManager.instance.attackContainer.GetComponent<ShipContainer>().freeze = true;
        GameManager.instance.shipsContainer.GetComponent<ShipContainer>().freeze = true;
        Invoke("Stop", time);
    }

    void Stop()
    {
        GameManager.instance.attackContainer.GetComponent<ShipContainer>().freeze = false;
        GameManager.instance.shipsContainer.GetComponent<ShipContainer>().freeze = false;
        Destroy(this);
    }

    private void OnDisable()
    {
        GameManager.instance.attackContainer.GetComponent<ShipContainer>().freeze = false;
        GameManager.instance.shipsContainer.GetComponent<ShipContainer>().freeze = false;

        Destroy(this);
    }
}
