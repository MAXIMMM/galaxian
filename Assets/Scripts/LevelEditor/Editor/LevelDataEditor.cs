﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CanEditMultipleObjects]
[CustomEditor(typeof(LevelsData))]
public class LevelDataEditor : Editor {

    private LevelsData levelsData;

    private void OnEnable()

    {
        levelsData = target as LevelsData;
    }

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Add"))
        {
            levelsData.levels.Add(new LevelConfig());
        }

        EditorGUI.indentLevel += 1;
        for (int i=0; i< levelsData.levels.Count; i++)
        {
            EditorGUILayout.BeginHorizontal("Box");

            levelsData.levels[i].fold = EditorGUILayout.Foldout(levelsData.levels[i].fold, levelsData.levels[i].name);

            if (GUILayout.Button("Delete", GUILayout.Width(50)))
            {
                Debug.Log("Delete");

                levelsData.levels.RemoveAt(i);
            }
            else
            {
                if (GUILayout.Button("UP", GUILayout.Width(50)))
                {
                    if (i > 0)
                    {
                        LevelConfig levelConfig = levelsData.levels[i];

                        levelsData.levels.Remove(levelConfig);

                        levelsData.levels.Insert(i - 1, levelConfig);
                    }
                    
                }
                if (GUILayout.Button("DOWN", GUILayout.Width(50)))
                {
                    if (i< levelsData.levels.Count-1)
                    {
                        LevelConfig levelConfig = levelsData.levels[i];

                        levelsData.levels.Remove(levelConfig);

                        levelsData.levels.Insert(i + 1, levelConfig);
                    }
                }

                EditorGUILayout.EndHorizontal();

                if (levelsData.levels[i].fold)
                {
                    EditorGUI.indentLevel += 1;

                    levelsData.levels[i].name = EditorGUILayout.TextField("Name", levelsData.levels[i].name);
                    levelsData.levels[i].nexus = (ScriptableObject)EditorGUILayout.ObjectField("Nexus", levelsData.levels[i].nexus, typeof(ScriptableObject), false);
                    EditorGUILayout.LabelField("");
                    levelsData.levels[i].background = (Sprite)EditorGUILayout.ObjectField("Background", levelsData.levels[i].background, typeof(Sprite), false);
                    EditorGUILayout.LabelField("");
                    levelsData.levels[i].music = (AudioClip)EditorGUILayout.ObjectField("Background", levelsData.levels[i].music, typeof(AudioClip), false);
                    levelsData.levels[i].separationTime = EditorGUILayout.FloatField("Separation Time", levelsData.levels[i].separationTime);

                    EditorGUI.indentLevel -= 1;
                }

            }
        }

        EditorGUI.indentLevel -= 1;

        EditorUtility.SetDirty(target);
    }
}
