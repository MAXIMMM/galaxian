﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ray : Weapon{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        lifeTime -= Time.deltaTime;

        if (lifeTime <= 0)
        {
            Destroy();
        }
        transform.position += 8f * transform.up * Time.deltaTime;

    }

    public override void OnPop()
    {
        gameObject.SetActive(true);
        lifeTime = 3f;
    }

    public override void OnPush()
    {
        //Debug.Log("Push");
        gameObject.SetActive(false);
    }
}
