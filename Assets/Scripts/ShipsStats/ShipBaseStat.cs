﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ShipBaseStat {

    [SerializeField]
    private string _name = "Base Stat";
    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private float _Health = 100;
    public float Health
    {
        get { return _Health; }
        set { _Health = value; }
    }

    //[SerializeField]
    //private float _shootSpeed = 500;
    //public float shootSpeed
    //{
    //    get { return _shootSpeed; }
    //    set { _shootSpeed = value; }
    //}

    [SerializeField]
    private float _moveSpeed = 30;
    public float moveSpeed
    {
        get { return _moveSpeed; }
        set { _moveSpeed = value; }
    }

    [SerializeField]
    private float _mobility = 15;
    public float mobility
    {
        get { return _mobility; }
        set { _mobility = value; }
    }

    public bool fold { get; set; }
}


