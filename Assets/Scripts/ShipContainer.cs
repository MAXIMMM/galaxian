﻿using UnityEngine;

public class ShipContainer : MonoBehaviour
{
    private Transform _player;

    public Transform player
    {
        get { return _player; }
        set { _player = value; }
    }

    public bool freeze { get; set; }

    private float minX;
    private float maxX;
    private float minY;
    private float maxY;

    public void SetRect(float minX, float maxX, float minY, float maxY)
    {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }



    private Vector2 lastPos;
    private Vector2 target;
    private float time, dt, dist;

    private Ship child;

    private void Start()
    {
        NewTarget();
        freeze = false;
    }

    public void AddShip(GameObject ship)
    {
        child = ship.GetComponent<Ship>();
        ship.transform.SetParent(transform);
        NewTarget();
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;

        Gizmos.DrawWireCube(new Vector3(minX + (maxX - minX) / 2f, minY + (maxY - minY) / 2f, 0f), new Vector3(maxX - minX, maxY - minY, 0f));

        Gizmos.DrawSphere(target, 0.1f);
    }

    public void NewTarget()
    {
        if (player != null)
        {
            minX = player.transform.position.x - 1;
            maxX = player.transform.position.x + 1;
            minY = player.transform.position.y + 1;
            maxY = player.transform.position.y + 3;
        }

        lastPos = transform.position;
        target = new Vector2(Random.Range(minX, maxX), Random.Range(minY, maxY));
        time = 0f;
        dt = 0f;

        dist = Vector2.Distance(lastPos, target);
    }

    // Update is called once per frame
    private void Update()
    {
        if (freeze) return;

        float speed = 1;

        if (child != null)
        {
            speed = child.shipData.moveSpeed;
        }

        dt += Time.deltaTime*speed;
        time += ((1 - time) * dt) * Time.deltaTime;
        transform.position = Vector2.Lerp(lastPos, target, time);

        if (time >= 0.95f)
        {
            NewTarget();
        }
    }
}