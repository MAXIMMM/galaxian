﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelsSystem", menuName = "Galaxy/New Levels System")]
public class LevelsData : ScriptableObject {

    [SerializeField]
    private List<LevelConfig> _levels;
    public List<LevelConfig> levels
    {
        get
        {
            if (_levels == null) _levels = new List<LevelConfig>();
            return _levels;
        }
        set { _levels = value; }
    }

}
