﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "BonusSystem", menuName = "Galaxy/New Bonus System")]
public class BonusSystem : ScriptableObject {

    [SerializeField]
    private List<BonusData> _bonuses;
    public List<BonusData> bonuses
    {
        get
        {
            if (_bonuses == null) _bonuses = new List<BonusData>();
            return _bonuses;
        }
        set { _bonuses = value; }
    }

}
