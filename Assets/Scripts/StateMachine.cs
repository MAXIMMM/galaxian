﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine<StateName> {

    private class State
    {
        public readonly StateName name;
        public readonly Action onStart;
        public readonly Action onStop;
        public readonly Action onUpdate;
        
        public State(StateName name, Action onStart, Action onUpdate, Action onStop)
        {
            this.onStart = onStart;
            this.onUpdate = onUpdate;
            this.onStop = onStop;
            this.name = name;
        }
    }

    private readonly Dictionary<StateName, State> stateDictionary;

    private State currentState;
    public StateName CurrentState
    {
        get
        {
            return currentState.name;
        }
        
        set
        {
            ChangeState(value);
        }
    }

    public StateMachine()
    {
        stateDictionary = new Dictionary<StateName, State>();
    }

    public void Update()
    {
        if (currentState != null && currentState.onUpdate != null)
        {
            currentState.onUpdate();
        }
    }

    public void AddState(StateName name, Action onStart, Action onUpdate, Action onStop)
    {
        stateDictionary[name] = new State(name, onStart, onUpdate, onStop);
    }

    public void AddState(StateName name, Action onStart, Action onUpdate)
    {
        AddState(name, onStart, onUpdate, null);
    }

    public void AddState(StateName name, Action onStart)
    {
        AddState(name, onStart, null);
    }

    public void AddState(StateName name)
    {
        AddState(name, null);
    }

    private void ChangeState(StateName newState)
    {
        if (currentState != null && currentState.onStop != null)
        {
            currentState.onStop();
        }

        currentState = stateDictionary[newState];

        if (currentState.onStart != null)
        {
            currentState.onStart();
        }
    }

}
