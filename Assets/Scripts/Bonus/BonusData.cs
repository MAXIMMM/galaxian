﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[System.Serializable]
public class BonusData {
    [SerializeField]
    private string _name = "Bonus Name";
    public string name
    {
        get { return _name; }
        set { _name = value; }
    }

    [SerializeField]
    private Object _script;
    public Object script
    {
        get { return _script; }
        set { _script = value; }
    }

    [SerializeField]
    private GameObject _prefab;
    public GameObject prefab
    {
        get { return _prefab; }
        set { _prefab = value; }
    }

    public bool fold { get; set; }
}
