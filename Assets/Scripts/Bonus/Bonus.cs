﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bonus : PoolingObject
{
    private BonusData _data;
    public BonusData data
    {
        get { return _data; }
        set { _data = value; }
    }

    public void GetBonus()
    {
        string type = data.script.name;
        GameManager.instance.playerShip.AddComponent(System.Type.GetType(type));
        SoundManager.instance.PlayGetBonus();
        Destroy();
    }

    public override void OnPush()
    {
        GameManager.instance.bonuses.Remove(gameObject);
        gameObject.SetActive(false);
    }

    public override void OnPop()
    {
        SoundManager.instance.PlayBonus();
        gameObject.SetActive(true);
    }

    public void Destroy()
    {
        GameManager.instance.objectsPool.Push(this);
    }
}
