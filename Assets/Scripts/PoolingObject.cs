﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PoolingObject: MonoBehaviour {

    private GameObject _key;
    public GameObject Key
    {
        get { return _key; }
        set { _key = value; }
    }


    public abstract void OnPush();
    public abstract void OnPop();

    public GameObject GetGameObject()
    {
        return gameObject;
    }
    
}
